#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/types.h>
#include <string.h> // memcpy , memset
#include "Restart.h"

int main() {
    in_port_t port = 6500;
    //char ipaddress[] = "127.0.0.1";
    char ipaddress[] = "luffarserv.miun.se";
    int sock;
    struct sockaddr_in serverAddr;
    char buf[100];
    char msg[] = "CHA:0:1:05:Nisse";

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        perror("Failed to create socket");

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(port);
    /* Convert address to binary form */
    struct addrinfo hints; /* in: hints */
    struct addrinfo *res; /* out: result */
    memset(&hints,0,sizeof(addrinfo)); // zero out the struct
    // Set relevant members
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    if(getaddrinfo(ipaddress,NULL,&hints,&res)!=0)
        return -1;

    struct sockaddr_in *saddrp = (sockaddr_in*)(res->ai_addr);
    /* Copy binary address */
    memcpy(&serverAddr.sin_addr.s_addr, &saddrp->sin_addr.s_addr,4);
    freeaddrinfo(res); // release memory allocated by getaddrinfo

    if((connect(sock,(sockaddr*)&serverAddr,sizeof(serverAddr))) == -1)
        perror("Failed to connect");
    /* Error handling, see Program 18.8 */
    /* Connection established, use r_write/r_read to communicate */
    int byteswritten = r_write(sock,msg,sizeof(msg));
    int bytesread = r_read(sock,buf,100);
    printf("Wrote: %d Read: %d\n%s\n",byteswritten,bytesread,buf);
    if(r_close(sock)==-1)
    perror("closing socket");
    return 0;
}
